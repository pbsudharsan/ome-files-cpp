/*
 * #%L
 * OME-FILES C++ library for image IO.
 * Copyright © 2006 - 2015 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * Copyright © 2019 Codelibre Consulting Limited
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <iostream>
#include <string>

#include <fmt/format.h>

#include <ome/files/FormatException.h>
#include <ome/files/FormatTools.h>
#include <ome/files/MetadataTools.h>
#include <ome/files/PixelProperties.h>
#include <ome/files/XMLTools.h>

#include <ome/compat/regex.h>

#ifdef OME_HAVE_XERCES_DOM
#include <ome/xerces-util/Platform.h>
#include <ome/xerces-util/String.h>
#include <ome/xerces-util/dom/Document.h>
#include <ome/xerces-util/dom/Element.h>
#include <ome/xerces-util/dom/NodeList.h>
#ifdef OME_HAVE_XALAN_XSLT
#include <ome/xalan-util/Platform.h>
#endif
#endif

#include <ome/xml/Document.h>
#include <ome/xml/OMETransform.h>
#include <ome/xml/version.h>

#include <ome/xml/meta/Convert.h>
#include <ome/xml/meta/MetadataException.h>
#include <ome/xml/meta/OMEXMLMetadataRoot.h>

#include <ome/xml/model/Annotation.h>
#include <ome/xml/model/Channel.h>
#include <ome/xml/model/Image.h>
#include <ome/xml/model/MapAnnotation.h>
#include <ome/xml/model/MetadataOnly.h>
#include <ome/xml/model/ModelException.h>
#include <ome/xml/model/OMEModel.h>
#include <ome/xml/model/OriginalMetadataAnnotation.h>
#include <ome/xml/model/Pixels.h>
#include <ome/xml/model/StructuredAnnotations.h>
#include <ome/xml/model/XMLAnnotation.h>
#include <ome/xml/model/primitives/OrderedMultimap.h>
#include <ome/xml/model/primitives/Timestamp.h>

#ifdef OME_HAVE_XERCES_DOM
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/sax/SAXException.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#elif OME_HAVE_QT5_DOM
#include <QtXml/QXmlDefaultHandler>
#include <QtXml/QXmlSimpleReader>
#endif

// Include last due to side effect of MPL vector limit setting which can change the default
#include <boost/lexical_cast.hpp>

using ome::xml::meta::Metadata;
using ome::xml::meta::MetadataException;
using ome::xml::meta::MetadataStore;
using ome::xml::meta::MetadataRetrieve;
using ome::xml::meta::MetadataRoot;
using ome::xml::meta::OMEXMLMetadata;
using ome::xml::meta::OMEXMLMetadataRoot;

using ome::xml::model::Image;
using ome::xml::model::MapAnnotation;
using ome::xml::model::MetadataOnly;
using ome::xml::model::ModelException;
using ome::xml::model::OMEModel;
using ome::xml::model::OriginalMetadataAnnotation;
using ome::xml::model::Pixels;
using ome::xml::model::StructuredAnnotations;
using ome::xml::model::XMLAnnotation;
using ome::xml::model::primitives::Timestamp;
using ome::xml::model::primitives::PositiveInteger;

using ome::xml::DOMDocument;
using ome::xml::DOMElement;
using ome::xml::DOMNode;
using ome::xml::DOMNodeList;
using ome::xml::DOMNamedNodeMap;

namespace
{

  /// Use default creation date?
  bool defaultCreationDate = false;

  template<typename T>
  void parseNodeValue(DOMNode& node,
                      T&       value)
  {
if (
#ifdef OME_HAVE_XERCES_DOM
    node
#elif OME_HAVE_QT5_DOM
    !node.isNull()
#endif
    )
      {
        try
          {
#ifdef OME_HAVE_XERCES_DOM
            value = boost::lexical_cast<T>(node.getNodeValue());
#elif OME_HAVE_QT5_DOM
            value = boost::lexical_cast<T>(node.nodeValue().toStdString());
#endif
          }
        catch (boost::bad_lexical_cast const&)
          {
            /// @todo Warn if parsing fails.
          }
      }
  }

  const std::string resolution_namespace("codelibre.net/ResolutionLevels");

  const ome::compat::regex schema_match("^http://www.openmicroscopy.org/Schemas/OME/(.*)$");

  class OMEXMLVersionParser :
#ifdef OME_HAVE_XERCES_DOM
    public xercesc::DefaultHandler
#elif OME_HAVE_QT5_DOM
    public QXmlDefaultHandler
#endif
  {
  public:
    OMEXMLVersionParser():
#ifdef OME_HAVE_XERCES_DOM
      xercesc::DefaultHandler()
#elif OME_HAVE_QT5_DOM
      QXmlDefaultHandler()
#endif
    {}

    virtual ~OMEXMLVersionParser() {}

#ifdef OME_HAVE_XERCES_DOM
    void
    startElement(const XMLCh* const         uri,
                 const XMLCh* const         localname,
                 const XMLCh* const         /* qname */,
                 const xercesc::Attributes& /* attrs */)
    {
      if (ome::common::xml::String(localname) == "OME")
        {
          std::string ns = ome::common::xml::String(uri);

          ome::compat::smatch found;

          if (ome::compat::regex_match(ns, found, schema_match))
            {
              version = found[1];
              throw xercesc::SAXException(ome::common::xml::String("Found schema version"));
            }
        }
    }
#elif OME_HAVE_QT5_DOM
    bool
    startElement(const QString&        uri,
                 const QString&        localname,
                 const QString&        /* qName */,
                 const QXmlAttributes& /* attrs */)
    {
      if (localname == QString::fromUtf8("OME"))
        {
          std::string ns = uri.toStdString();

          ome::compat::smatch found;

          if (ome::compat::regex_match(ns, found, schema_match))
            {
              version = found[1];
              return false; // terminate processing
            }
        }

      return true;
    }
#endif

    std::string
    getVersion() const
    {
      return version;
    }

  private:
    std::string version;
  };

}

namespace ome
{
  namespace files
  {

    std::string
    createID(std::string const&  type,
             dimension_size_type idx)
    {
      return fmt::format("{0}:{1}", type, idx);
    }

    std::string
    createID(std::string const&  type,
             dimension_size_type idx1,
             dimension_size_type idx2)
    {
      return fmt::format("{0}:{1}:{2}", type, idx1, idx2);
    }

    std::string
    createID(std::string const&  type,
             dimension_size_type idx1,
             dimension_size_type idx2,
             dimension_size_type idx3)
    {
      return fmt::format("{0}:{1}:{2}:{3}", type, idx1, idx2, idx3);
    }

    std::string
    createID(std::string const&  type,
             dimension_size_type idx1,
             dimension_size_type idx2,
             dimension_size_type idx3,
             dimension_size_type idx4)
    {
      return fmt::format("{0}:{1}:{2}:{3}:{4}", type, idx1, idx2, idx3, idx4);
    }

    std::shared_ptr<::ome::xml::meta::OMEXMLMetadata>
    createOMEXMLMetadata(DOMDocument& document)
    {
#ifdef OME_HAVE_XALAN_XSLT
      ome::common::xsl::Platform xslplat;
#endif
      DOMDocument upgraded_doc;
      DOMElement docroot;

      if (getModelVersion(document) != OME_XML_MODEL_VERSION)
        {
          // Transform to latest
          ome::xml::OMEEntityResolver entity_resolver;
          ome::xml::OMETransformResolver transform_resolver;

          std::string xml;
#ifdef OME_HAVE_XERCES_DOM
          ome::common::xml::dom::writeDocument(document, xml);
#elif OME_HAVE_QT5_DOM
          // Only handles the case of string output.
          xml = document.toString().toStdString();
#endif

          std::string upgraded_xml;
          ome::xml::transform(OME_XML_MODEL_VERSION, xml, upgraded_xml,
                              entity_resolver, transform_resolver);

          try
            {
#ifdef OME_HAVE_XERCES_DOM
              upgraded_doc = ome::xml::createDocument(upgraded_xml, ome::common::xml::dom::ParseParameters(),
                                                      "OME-XML text (current schema)");
#elif OME_HAVE_QT5_DOM
              upgraded_doc = ome::xml::createDocument(upgraded_xml,
                                                      "OME-XML text (current schema)");
#endif
            }
          catch (const std::runtime_error&) // retry without strict validation
            {
#ifdef OME_HAVE_XERCES_DOM
              ome::common::xml::dom::ParseParameters params;
              params.doSchema = false;
              params.validationSchemaFullChecking = false;
              upgraded_doc = ome::xml::createDocument(upgraded_xml, params, "Broken OME-XML");
#elif OME_HAVE_QT5_DOM
              throw; // QtXML isn't validating so this should never be reached.
#endif
            }
#ifdef OME_HAVE_XERCES_DOM
          docroot = upgraded_doc.getDocumentElement();
#elif OME_HAVE_QT5_DOM
          docroot = upgraded_doc.documentElement();
#endif
        }
      else
        {
#ifdef OME_HAVE_XERCES_DOM
          docroot = document.getDocumentElement();
#elif OME_HAVE_QT5_DOM
          docroot = document.documentElement();
#endif
        }

      std::shared_ptr<::ome::xml::meta::OMEXMLMetadata> meta(std::make_shared<::ome::xml::meta::OMEXMLMetadata>());
      ome::xml::model::detail::OMEModel model;
      std::shared_ptr<ome::xml::meta::OMEXMLMetadataRoot> root(std::dynamic_pointer_cast<ome::xml::meta::OMEXMLMetadataRoot>(meta->getRoot()));
      root->update(docroot, model);
      model.resolveReferences();

      return meta;
    }

    std::shared_ptr<::ome::xml::meta::OMEXMLMetadata>
    createOMEXMLMetadata(const ome::compat::filesystem::path& file)
    {
      // Parse OME-XML into DOM Document.
#ifdef OME_HAVE_XERCES_DOM
      ome::common::xml::Platform xmlplat;
#ifdef OME_HAVE_XALAN_XSLT
      ome::common::xsl::Platform xslplat;
#endif
#endif
      DOMDocument doc;
      try
        {
          doc = ome::xml::createDocument(file);
        }
      catch (const std::runtime_error&) // retry without strict validation
        {
#ifdef OME_HAVE_XERCES_DOM
          ome::common::xml::dom::ParseParameters params;
          params.doSchema = false;
          params.validationSchemaFullChecking = false;
          doc = ome::xml::createDocument(file, params);
#elif OME_HAVE_QT5_DOM
          throw; // QtXML isn't validating so this should never be reached.
#endif
        }
      return createOMEXMLMetadata(doc);
    }

    std::shared_ptr<::ome::xml::meta::OMEXMLMetadata>
    createOMEXMLMetadata(const std::string& text)
    {
      // Parse OME-XML into DOM Document.
#ifdef OME_HAVE_XERCES_DOM
      ome::common::xml::Platform xmlplat;
#ifdef OME_HAVE_XALAN_XSLT
      ome::common::xsl::Platform xslplat;
#endif
#endif
      DOMDocument doc;
      try
        {
#ifdef OME_HAVE_XERCES_DOM
          doc = ome::xml::createDocument(text, ome::common::xml::dom::ParseParameters(),
                                         "OME-XML text");
#elif OME_HAVE_QT5_DOM
          doc = ome::xml::createDocument(text,
                                         "OME-XML text");
#endif
        }
      catch (const std::runtime_error&) // retry without strict validation
        {
#ifdef OME_HAVE_XERCES_DOM
          ome::common::xml::dom::ParseParameters params;
          params.doSchema = false;
          params.validationSchemaFullChecking = false;
          doc = ome::xml::createDocument(text, params, "Broken OME-XML text");
#elif OME_HAVE_QT5_DOM
          throw; // QtXML isn't validating so this should never be reached.
#endif
        }
      return createOMEXMLMetadata(doc);
    }

    std::shared_ptr<::ome::xml::meta::OMEXMLMetadata>
    createOMEXMLMetadata(std::istream& stream)
    {
      // Parse OME-XML into DOM Document.
#ifdef OME_HAVE_XERCES_DOM
      ome::common::xml::Platform xmlplat;
#ifdef OME_HAVE_XALAN_XSLT
      ome::common::xsl::Platform xslplat;
#endif
#endif
      DOMDocument doc;
      try
        {
#ifdef OME_HAVE_XERCES_DOM
          doc = ome::xml::createDocument(stream, ome::common::xml::dom::ParseParameters(),
                                         "OME-XML stream");
#elif OME_HAVE_QT5_DOM
          doc = ome::xml::createDocument(stream,
                                         "OME-XML stream");
#endif
        }
      catch (const std::runtime_error&) // retry without strict validation
        {
#ifdef OME_HAVE_XERCES_DOM
          ome::common::xml::dom::ParseParameters params;
          params.doSchema = false;
          params.validationSchemaFullChecking = false;
          doc = ome::xml::createDocument(stream, params, "Broken OME-XML stream");
#elif OME_HAVE_QT5_DOM
          throw; // QtXML isn't validating so this should never be reached.
#endif
        }
      return createOMEXMLMetadata(doc);
    }

    std::shared_ptr<::ome::xml::meta::OMEXMLMetadata>
    createOMEXMLMetadata(const FormatReader& reader,
                         bool                doPlane,
                         bool                doImageName)
    {
      std::shared_ptr<OMEXMLMetadata> metadata(std::make_shared<OMEXMLMetadata>());
      std::shared_ptr<MetadataStore> store(std::static_pointer_cast<MetadataStore>(metadata));
      fillMetadata(*store, reader, doPlane, doImageName);
      return metadata;
    }

    std::shared_ptr<::ome::xml::meta::MetadataRoot>
    createOMEXMLRoot(const std::string& document)
    {
      /// @todo Implement model transforms.

      std::shared_ptr<::ome::xml::meta::OMEXMLMetadata> meta(std::dynamic_pointer_cast<::ome::xml::meta::OMEXMLMetadata>(createOMEXMLMetadata(document)));
      return meta ? meta->getRoot() : std::shared_ptr<::ome::xml::meta::MetadataRoot>();
    }

    std::shared_ptr<::ome::xml::meta::OMEXMLMetadata>
    getOMEXMLMetadata(std::shared_ptr<::ome::xml::meta::MetadataRetrieve>& retrieve)
    {
      std::shared_ptr<OMEXMLMetadata> ret;

      if (retrieve)
        {
          std::shared_ptr<OMEXMLMetadata> omexml(std::dynamic_pointer_cast<OMEXMLMetadata>(retrieve));
          if (omexml)
            {
              ret = omexml;
            }
          else
            {
              ret = std::shared_ptr<OMEXMLMetadata>(std::make_shared<OMEXMLMetadata>());
              ome::xml::meta::convert(*retrieve, *ret);
            }
        }

      return ret;
    }

    std::string
    getOMEXML(::ome::xml::meta::OMEXMLMetadata& omexml,
              bool                              validate)
    {
      std::string xml(omexml.dumpXML());

      if (validate && !validateOMEXML(xml))
        throw std::runtime_error("Invalid OME-XML document");

      return xml;
    }

    bool
    validateOMEXML(const std::string& document)
    {
      return validateXML(document, "OME-XML document for validation");
    }

    bool
    validateModel(::ome::xml::meta::Metadata& meta,
                  bool                        correct)
    {
      bool valid = true;

      const dimension_size_type seriesCount = meta.getImageCount();

      for (dimension_size_type series = 0U; series < seriesCount; ++series)
        {
          const dimension_size_type sizeX = meta.getPixelsSizeX(series);
          const dimension_size_type sizeY = meta.getPixelsSizeY(series);
          const dimension_size_type sizeZ = meta.getPixelsSizeZ(series);
          const dimension_size_type sizeT = meta.getPixelsSizeT(series);
          const dimension_size_type sizeC = meta.getPixelsSizeC(series);
          if (!sizeX || !sizeY || !sizeZ || !sizeT)
            {
              valid = false;
              if (!correct)
                break;

              std::string fs = fmt::format("Invalid image dimensionality for Image {0}: "
                                           "SizeX={1}, SizeY={2}, SizeZ={3}, SizeT={4}, SizeC={5}",
                                           series, sizeX, sizeY, sizeZ, sizeT, sizeC);
              throw FormatException(fs);
            }

          // If no Channel objects are defined, create with 1
          // SamplePerPixel.
          if (meta.getChannelCount(series) == 0 && sizeC)
            {
              valid = false;
              if (!correct)
                {
                  break;
                }
              else
                {
                  for (dimension_size_type c = 0; c < sizeC; ++c)
                    meta.setChannelSamplesPerPixel(1U, series, c);
                }
            }

          const dimension_size_type effC = meta.getChannelCount(series);
          // Sum of all set SamplesPerPixel
          dimension_size_type realSizeC = 0U;
          std::vector<dimension_size_type> badChannels;
          for (dimension_size_type c = 0; c < effC; ++c)
            {
              dimension_size_type samples = 1U;

              try
                {
                  samples = meta.getChannelSamplesPerPixel(series, c);
                  realSizeC += samples;
                }
              catch (const MetadataException&)
                {
                  badChannels.push_back(c);
                }
            }

          // If all samples add up to SizeC then the Channel
          // metadata is correct; do nothing.
          if (sizeC && realSizeC &&
              sizeC == realSizeC &&
              badChannels.empty())
            continue;

          valid = false;
          if (!correct)
            break;

          if (sizeC == 0 && realSizeC == 0)
            {
              // No channels or samples defined; default to one
              // sample per channel.
              meta.setPixelsSizeC(effC, series);
              for (dimension_size_type c = 0; c < effC; ++c)
                meta.setChannelSamplesPerPixel(1U, series, c);
            }
          else if (realSizeC > 0 &&
                   badChannels.empty())
            {
              // All samples set and no bad channels; update SizeC
              // to reflect the sample total.
              meta.setPixelsSizeC(realSizeC, series);
            }
          else if (sizeC > 0 &&
                   sizeC >= realSizeC &&
                   !badChannels.empty())
            {
              // Some or all channels are unset.  If the unallocated
              // samples are evenly divisible between the unset
              // channels, assign.
              const dimension_size_type allocSamples = realSizeC;
              const dimension_size_type unallocSamples = sizeC >= realSizeC ? sizeC - allocSamples : 0U;
              const dimension_size_type splitSamples = unallocSamples / badChannels.size();
              const dimension_size_type badSamples = unallocSamples % badChannels.size();

              // No point guessing since we can't make a sensible
              // sample allocation; bail out now.
              if (!splitSamples || badSamples || sizeC < realSizeC)
                {
                  std::string fs = fmt::format("Unable to correct invalid ChannelSamplesPerPixel in Image #{0}: "
                                               "{1} channel(s) set, "
                                               "{2} channel(s) unset, "
                                               "{3} sample(s) unallocated",
                                               series,
                                               (effC - badChannels.size()),
                                               badChannels.size(),
                                               sizeC - realSizeC);
                  throw FormatException(fs);
                }

              for (const auto& c : badChannels)
                meta.setChannelSamplesPerPixel(splitSamples, series, c);
            }
        }

      return valid;
    }

    void
    fillMetadata(::ome::xml::meta::MetadataStore& store,
                 const FormatReader&              reader,
                 bool                             doPlane,
                 bool                             doImageName)
    {
      dimension_size_type oldseries = reader.getSeries();

      for (dimension_size_type s = 0; s < reader.getSeriesCount(); ++s)
        {
          reader.setSeries(s);

          const boost::optional<ome::compat::filesystem::path>& cfile(reader.getCurrentFile());

          std::ostringstream nos;
          if (doImageName && !!cfile)
            {
              nos << (*cfile).string();
              if (reader.getSeriesCount() > 1)
                nos << " #" << (s + 1);
            }
          std::string imageName = nos.str();

          std::string pixelType = reader.getPixelType();

          if (!imageName.empty())
            store.setImageID(createID("Image", s), s);
          if (!!cfile)
            setDefaultCreationDate(store, s, *cfile);

          fillPixels(store, reader);

          try
            {
              OMEXMLMetadata& omexml(dynamic_cast<OMEXMLMetadata&>(store));
              if (omexml.getTiffDataCount(s) == 0 &&
                  omexml.getPixelsBinDataCount(s) == 0)
                addMetadataOnly(omexml, s);
            }
          catch (const std::bad_cast&)
            {
            }

          if (doPlane)
            {
              for (dimension_size_type p = 0; p < reader.getImageCount(); ++p)
                {
                  std::array<dimension_size_type, 3> coords = reader.getZCTCoords(p);
                  // The cast to int here is nasty, but the data model
                  // isn't using unsigned types…
                  store.setPlaneTheZ(static_cast<int>(coords[0]), s, p);
                  store.setPlaneTheC(static_cast<int>(coords[1]), s, p);
                  store.setPlaneTheT(static_cast<int>(coords[2]), s, p);
                }
            }

        }

      reader.setSeries(oldseries);
    }

    void
    fillMetadata(::ome::xml::meta::MetadataStore&                 store,
                 const std::vector<std::shared_ptr<CoreMetadata>> seriesList,
                 bool                                             doPlane)
    {
      dimension_size_type s = 0U;
      for (std::vector<std::shared_ptr<CoreMetadata>>::const_iterator i = seriesList.begin();
           i != seriesList.end();
           ++i, ++s)
        {
          std::string pixelType = (*i)->pixelType;

          store.setImageID(createID("Image", s), s);

          fillPixels(store, **i, s);

          try
            {
              OMEXMLMetadata& omexml(dynamic_cast<OMEXMLMetadata&>(store));
              if (omexml.getTiffDataCount(s) == 0 &&
                  omexml.getPixelsBinDataCount(s) == 0)
                addMetadataOnly(omexml, s);
            }
          catch (const std::bad_cast&)
            {
            }

          if (doPlane)
            {
              for (dimension_size_type p = 0; p < (*i)->imageCount; ++p)
                {
                  dimension_size_type sizeZT = (*i)->sizeZ * (*i)->sizeT;
                  dimension_size_type effSizeC = 1U;
                  if (sizeZT)
                    effSizeC = (*i)->imageCount / sizeZT;

                  std::array<dimension_size_type, 3> coords =
                    getZCTCoords((*i)->dimensionOrder,
                                 (*i)->sizeZ,
                                 effSizeC,
                                 (*i)->sizeT,
                                 (*i)->imageCount,
                                 p);
                  // The cast to int here is nasty, but the data model
                  // isn't using unsigned types…
                  store.setPlaneTheZ(static_cast<int>(coords[0]), s, p);
                  store.setPlaneTheC(static_cast<int>(coords[1]), s, p);
                  store.setPlaneTheT(static_cast<int>(coords[2]), s, p);
                }
            }
        }
    }

    void
    fillAllPixels(::ome::xml::meta::MetadataStore& store,
                  const FormatReader&              reader)
    {
      dimension_size_type oldseries = reader.getSeries();
      for (dimension_size_type s = 0; s < reader.getSeriesCount(); ++s)
        {
          reader.setSeries(s);
          fillPixels(store, reader);
        }
      reader.setSeries(oldseries);
    }

    void
    fillPixels(::ome::xml::meta::MetadataStore& store,
               const FormatReader&              reader)
    {
      dimension_size_type series = reader.getSeries();

      store.setPixelsID(createID("Pixels", series), series);
      store.setPixelsBigEndian(!reader.isLittleEndian(), series);
      store.setPixelsSignificantBits(reader.getBitsPerPixel(), series);
      store.setPixelsDimensionOrder(reader.getDimensionOrder(), series);
      store.setPixelsInterleaved(reader.isInterleaved(), series);
      store.setPixelsType(reader.getPixelType(), series);

      // The cast to int here is nasty, but the data model isn't using
      // unsigned types…
      store.setPixelsSizeX(static_cast<PositiveInteger::value_type>(reader.getSizeX()), series);
      store.setPixelsSizeY(static_cast<PositiveInteger::value_type>(reader.getSizeY()), series);
      store.setPixelsSizeZ(static_cast<PositiveInteger::value_type>(reader.getSizeZ()), series);
      store.setPixelsSizeT(static_cast<PositiveInteger::value_type>(reader.getSizeT()), series);
      store.setPixelsSizeC(static_cast<PositiveInteger::value_type>(reader.getSizeC()), series);

      dimension_size_type effSizeC = reader.getEffectiveSizeC();
      for (dimension_size_type c = 0; c < effSizeC; ++c)
        {
          store.setChannelID(createID("Channel", series, c), series, c);
          store.setChannelSamplesPerPixel(static_cast<PositiveInteger::value_type>(reader.getRGBChannelCount(c)), series, c);
        }

    }

    void
    fillPixels(::ome::xml::meta::MetadataStore& store,
               const CoreMetadata&              seriesMetadata,
               dimension_size_type              series)
    {
      store.setPixelsID(createID("Pixels", series), series);
      store.setPixelsBigEndian(!seriesMetadata.littleEndian, series);
      store.setPixelsSignificantBits(seriesMetadata.bitsPerPixel, series);
      store.setPixelsDimensionOrder(seriesMetadata.dimensionOrder, series);
      store.setPixelsInterleaved(seriesMetadata.interleaved, series);
      store.setPixelsType(seriesMetadata.pixelType, series);

      // The cast to int here is nasty, but the data model isn't using
      // unsigned types…
      store.setPixelsSizeX(static_cast<PositiveInteger::value_type>(seriesMetadata.sizeX), series);
      store.setPixelsSizeY(static_cast<PositiveInteger::value_type>(seriesMetadata.sizeY), series);
      store.setPixelsSizeZ(static_cast<PositiveInteger::value_type>(seriesMetadata.sizeZ), series);
      store.setPixelsSizeT(static_cast<PositiveInteger::value_type>(seriesMetadata.sizeT), series);
      store.setPixelsSizeC(static_cast<PositiveInteger::value_type>
                           (std::accumulate(seriesMetadata.sizeC.begin(), seriesMetadata.sizeC.end(),
                                            dimension_size_type(0))), series);

      dimension_size_type effSizeC = seriesMetadata.sizeC.size();

      for (dimension_size_type c = 0; c < effSizeC; ++c)
        {
          dimension_size_type rgbC = seriesMetadata.sizeC.at(c);

          store.setChannelID(createID("Channel", series, c), series, c);
          store.setChannelSamplesPerPixel(static_cast<PositiveInteger::value_type>(rgbC), series, c);
        }

    }

    void
    addMetadataOnly(::ome::xml::meta::OMEXMLMetadata& omexml,
                    dimension_size_type               series,
                    bool                              resolve)
    {
      if (resolve)
        omexml.resolveReferences();
      std::shared_ptr<MetadataRoot> root(omexml.getRoot());
      std::shared_ptr<OMEXMLMetadataRoot> omexmlroot(std::dynamic_pointer_cast<OMEXMLMetadataRoot>(root));
      if (omexmlroot)
        {
          std::shared_ptr<Image> image = omexmlroot->getImage(series);
          if (image)
            {
              std::shared_ptr<Pixels> pixels = image->getPixels();
              if (pixels)
                {
                  std::shared_ptr<MetadataOnly> meta(std::make_shared<MetadataOnly>());
                  pixels->setMetadataOnly(meta);
                }
            }
        }
    }

    Modulo
    getModuloAlongZ(const ::ome::xml::meta::OMEXMLMetadata& omexml,
                    dimension_size_type                     image)
    {
      return getModulo(omexml, "ModuloAlongZ", image);
    }

    Modulo
    getModuloAlongT(const ::ome::xml::meta::OMEXMLMetadata& omexml,
                    dimension_size_type                     image)
    {
      return getModulo(omexml, "ModuloAlongT", image);
    }

    Modulo
    getModuloAlongC(const ::ome::xml::meta::OMEXMLMetadata& omexml,
                    dimension_size_type                     image)
    {
      return getModulo(omexml, "ModuloAlongC", image);
    }

    Modulo
    getModulo(const ::ome::xml::meta::OMEXMLMetadata& omexml,
              const std::string&                      tag,
              dimension_size_type                     image)
    {
      // @todo Implement Modulo retrieval.
      ::ome::xml::meta::OMEXMLMetadata& momexml(const_cast<::ome::xml::meta::OMEXMLMetadata&>(omexml));

      std::shared_ptr<OMEXMLMetadataRoot> root =
        std::dynamic_pointer_cast<OMEXMLMetadataRoot>(momexml.getRoot());
      if (!root) // Should never occur
        throw std::logic_error("OMEXMLMetadata does not have an OMEXMLMetadataRoot");

      std::shared_ptr<::ome::xml::model::Image> mimage(root->getImage(image));
      if (!mimage)
        throw std::runtime_error("Image does not exist in OMEXMLMetadata");

      for (::ome::xml::meta::MetadataStore::index_type i = 0;
           i < mimage->sizeOfLinkedAnnotationList();
           ++i)
        {
          std::shared_ptr<::ome::xml::model::Annotation> annotation(mimage->getLinkedAnnotation(i));
          std::shared_ptr<::ome::xml::model::XMLAnnotation> xmlannotation(std::dynamic_pointer_cast<::ome::xml::model::XMLAnnotation>(annotation));
          if (xmlannotation)
            {
              try
                {
#ifdef OME_HAVE_XERCES_DOM
                  ome::common::xml::Platform xmlplat;
#endif
                  DOMDocument xmlroot(::ome::xml::createDocument(xmlannotation->getValue()));
#ifdef OME_HAVE_XERCES_DOM
                  DOMNodeList nodes(xmlroot.getElementsByTagName(tag));
#elif OME_HAVE_QT5_DOM
                  DOMNodeList nodes(xmlroot.elementsByTagName(QString::fromUtf8(tag.c_str())));
#endif

                  Modulo m(tag.substr(tag.size() ? tag.size() - 1 : 0));

                  if (nodes.size() > 0)
                    {
#ifdef OME_HAVE_XERCES_DOM
                      DOMElement modulo(nodes.at(0));
                      DOMNamedNodeMap attrs(modulo.getAttributes());


                      DOMNode start = attrs.getNamedItem("Start");
                      DOMNode end = attrs.getNamedItem("End");
                      DOMNode step = attrs.getNamedItem("Step");
                      DOMNode type = attrs.getNamedItem("Type");
                      DOMNode typeDescription = attrs.getNamedItem("TypeDescription");
                      DOMNode unit = attrs.getNamedItem("Unit");
#elif OME_HAVE_QT5_DOM
                      DOMElement modulo(nodes.at(0).toElement());
                      DOMNamedNodeMap attrs(modulo.attributes());


                      DOMNode start = attrs.namedItem(QString::fromUtf8("Start"));
                      DOMNode end = attrs.namedItem(QString::fromUtf8("End"));
                      DOMNode step = attrs.namedItem(QString::fromUtf8("Step"));
                      DOMNode type = attrs.namedItem(QString::fromUtf8("Type"));
                      DOMNode typeDescription = attrs.namedItem(QString::fromUtf8("TypeDescription"));
                      DOMNode unit = attrs.namedItem(QString::fromUtf8("Unit"));
#endif

                      parseNodeValue(start, m.start);
                      parseNodeValue(end, m.end);
                      parseNodeValue(step, m.step);
                      parseNodeValue(type, m.type);
                      parseNodeValue(typeDescription, m.typeDescription);
                      parseNodeValue(unit, m.unit);

#ifdef OME_HAVE_XERCES_DOM
                      DOMNodeList labels = modulo.getElementsByTagName("Label");
#elif OME_HAVE_QT5_DOM
                      DOMNodeList labels = modulo.elementsByTagName("Label");
#endif
                      if (
#ifdef OME_HAVE_XERCES_DOM
                          labels && !labels.empty()
#elif OME_HAVE_QT5_DOM
                          !labels.isEmpty()
#endif
                          )
                        {
                          for (
#ifdef OME_HAVE_XERCES_DOM
                               auto& node : labels
#elif OME_HAVE_QT5_DOM
                               int i = 0;
                               i < labels.count();
                               ++i
#endif
                               )
                            {
                              std::string text;
#ifdef OME_HAVE_XERCES_DOM
                              text = node.getTextContent();
#elif OME_HAVE_QT5_DOM
                              QDomNode node = labels.at(i).firstChild();
                              if (!node.isNull())
                                {
                                  QDomText text_content = node.toText();
                                  if (!text_content.isNull())
                                    text = text_content.data().toStdString();
                                }
#endif
                              m.labels.push_back(text);
                            }
                        }

                    }
                  return m;
                }
              catch (...)
                {
                  throw std::runtime_error("Error parsing Modulo annotation");
                }
            }
        }

      throw std::runtime_error("Modulo annotation does not exist in OMEXMLMetadata");
    }

    void
    addResolutions(::ome::xml::meta::MetadataStore& store,
                   dimension_size_type              series,
                   const ResolutionList&            resolutions)
    {
      using OM = ome::xml::model::primitives::OrderedMultimap;
      OM map;

      if (resolutions.size() > 0)
        {
          int count = 1;
          for (const auto& r : resolutions)
            {
              std::ostringstream os;
              os << r[0] << ' ' << r[1] << ' ' << r[2];

              map.get<1>().insert(OM::value_type(boost::lexical_cast<std::string>(count), os.str()));
              ++count;
            }

          MetadataStore::index_type ma_idx = 0;

          MetadataRetrieve& retrieve(dynamic_cast<MetadataRetrieve&>(store));
          try
            {
              ma_idx = retrieve.getMapAnnotationCount();
            }
          catch (const ome::xml::meta::MetadataException &)
            {
              // StructuredAnnotations does not exist
            }

          std::string annotation_id = createID("Annotation:Resolutions", ma_idx);
          store.setMapAnnotationID(annotation_id, ma_idx);
          store.setMapAnnotationNamespace(resolution_namespace, ma_idx);
          store.setMapAnnotationValue(map, ma_idx);

          store.setImageAnnotationRef(annotation_id, series,
                                      retrieve.getImageAnnotationRefCount(series));

          auto& omexml(dynamic_cast<OMEXMLMetadata&>(store));
          omexml.resolveReferences(); // Not resolved automatically.
        }
    }

    void
    addResolutions(::ome::xml::meta::MetadataStore& store,
                   const MetadataList<Resolution>&  resolutions)
    {
      for (dimension_size_type i = 0; i < resolutions.size(); ++i)
        {
          if (resolutions[i].size() > 0)
            addResolutions(store, i, resolutions[i]);
        }
    }

    ResolutionList
    getResolutions(::ome::xml::meta::MetadataRetrieve& retrieve,
                   dimension_size_type                 image)
    {
      ResolutionList list;

      ::ome::xml::meta::OMEXMLMetadata& momexml(dynamic_cast<::ome::xml::meta::OMEXMLMetadata&>(retrieve));

      std::shared_ptr<OMEXMLMetadataRoot> root =
        std::dynamic_pointer_cast<OMEXMLMetadataRoot>(momexml.getRoot());
      if (!root) // Should never occur
        throw std::logic_error("OMEXMLMetadata does not have an OMEXMLMetadataRoot");

      std::shared_ptr<::ome::xml::model::Image> mimage(root->getImage(image));
      if (!mimage)
        throw std::runtime_error("Image does not exist in OMEXMLMetadata");

      auto mapannotation = getAnnotation<Image,MapAnnotation>(mimage, resolution_namespace);
      if (mapannotation)
        {
          auto map = mapannotation->getValue();

          std::map<dimension_size_type, Resolution> nmap;

          for(const auto& elem : map)
            {
              dimension_size_type r, x, y, z;
              std::istringstream k(elem.first);
              k >> r;
              std::istringstream v(elem.second);
              v >> x >> y >> z;

              if (!k || !v)
                {
                  nmap.clear();
                  break;
                }

              nmap.insert({{r}, {x, y, z}});
            }

          for (auto& elem : nmap)
            list.emplace_back(elem.second);
        }

      return list;
    }

    MetadataList<Resolution>
    getResolutions(::ome::xml::meta::MetadataRetrieve& retrieve)
    {
      MetadataList<Resolution> ret;

      for (MetadataStore::index_type image = 0;
           image < retrieve.getImageCount();
           ++image)
        {
          ret.push_back(getResolutions(retrieve, image));
        }

      return ret;
    }

    MetadataList<Resolution>
    getResolutions(const FormatReader& reader)
    {
      dimension_size_type oldseries = reader.getSeries();
      dimension_size_type oldresolution = reader.getResolution();

      MetadataList<Resolution> resolutions;

      dimension_size_type ic = reader.getSeriesCount();
      resolutions.resize(ic);
      for (dimension_size_type i = 0 ; i < ic; ++i)
        {
          reader.setSeries(i);
          dimension_size_type rc = reader.getResolutionCount();
          resolutions[i].resize(rc - 1);
          for (dimension_size_type r = 0 ; r < rc; ++r)
            {
              if (r)
                {
                  reader.setResolution(r);
                  resolutions[i][r - 1] = {reader.getSizeX(), reader.getSizeY(), reader.getSizeZ()};
                }
            }
        }

      reader.setSeries(oldseries);
      reader.setResolution(oldresolution);

      return resolutions;
    }

    void
    removeResolutions(::ome::xml::meta::MetadataStore& store,
                      dimension_size_type              series)
    {
      ::ome::xml::meta::OMEXMLMetadata& momexml(dynamic_cast<::ome::xml::meta::OMEXMLMetadata&>(store));

      std::shared_ptr<OMEXMLMetadataRoot> root =
        std::dynamic_pointer_cast<OMEXMLMetadataRoot>(momexml.getRoot());
      if (!root) // Should never occur
        throw std::logic_error("OMEXMLMetadata does not have an OMEXMLMetadataRoot");

      std::shared_ptr<::ome::xml::model::Image> mimage(root->getImage(series));
      if (!mimage)
        throw std::runtime_error("Image does not exist in OMEXMLMetadata");

      auto mapannotation = getAnnotation<Image,MapAnnotation>(mimage, resolution_namespace);

      removeAnnotation<Image,MapAnnotation>(mimage, resolution_namespace);

      auto sa = root->getStructuredAnnotations();
      if (sa)
        sa->removeMapAnnotation(mapannotation);
    }

    void
    removeResolutions(::ome::xml::meta::MetadataStore& store)
    {
      ::ome::xml::meta::OMEXMLMetadata& momexml(dynamic_cast<::ome::xml::meta::OMEXMLMetadata&>(store));

      for (MetadataStore::index_type image = 0;
           image < momexml.getImageCount();
           ++image)
        {
          removeResolutions(store, image);
        }
    }

    void
    verifyMinimum(::ome::xml::meta::MetadataRetrieve& retrieve,
                  dimension_size_type                 series)
    {
      // The Java equivalent of this function checks whether various
      // properties are null.  In the C++ implementation, most of the
      // properties checked can not be null (they are plain value
      // types) so the checks are not performed for these cases.  For
      // the string values, these may be empty (if unset), so this is
      // checked for in place of being null.

      try
        {
          MetadataStore& store(dynamic_cast<MetadataStore&>(retrieve));
          if (!store.getRoot())
            throw FormatException("Metadata object has null root; call createRoot() first");
        }
      catch (const std::bad_cast&)
        {
        }

      if (retrieve.getImageID(series).empty())
        {
          std::string fs = fmt::format("Image ID #{0} is empty", series);
          throw FormatException(fs);
        }

      if (retrieve.getPixelsID(series).empty())
        {
          std::string fs = fmt::format("Pixels ID #{0} is empty", series);
          throw FormatException(fs);
        }

      for (Metadata::index_type channel = 0;
           channel < retrieve.getChannelCount(series);
           ++channel)
        {
          if (retrieve.getChannelID(series, channel).empty())
            {
              std::string fs = fmt::format("Channel ID #{0} in Image #{1} is empty", channel, series);
              throw FormatException(fs);
            }
        }
    }

    void
    removeBinData(::ome::xml::meta::OMEXMLMetadata& omexml)
    {
      omexml.resolveReferences();
      std::shared_ptr<ome::xml::meta::OMEXMLMetadataRoot> root(std::dynamic_pointer_cast<ome::xml::meta::OMEXMLMetadataRoot>(omexml.getRoot()));
      if (root)
        {
          std::vector<std::shared_ptr<ome::xml::model::Image>>& images(root->getImageList());
          for(std::vector<std::shared_ptr<ome::xml::model::Image>>::const_iterator image = images.begin();
              image != images.end();
              ++image)
            {
              std::shared_ptr<ome::xml::model::Pixels> pixels((*image)->getPixels());
              if (pixels)
                {
                  // Note a copy not a reference to avoid iterator
                  // invalidation during removal.
                  std::vector<std::shared_ptr<ome::xml::model::BinData>> binData(pixels->getBinDataList());
                  for (auto& bin : binData)
                    {
                      pixels->removeBinData(bin);
                    }
                  std::shared_ptr<ome::xml::model::MetadataOnly> metadataOnly;
                  pixels->setMetadataOnly(metadataOnly);
                }
            }
        }
    }

    void
    removeTiffData(::ome::xml::meta::OMEXMLMetadata& omexml)
    {
      omexml.resolveReferences();
      std::shared_ptr<ome::xml::meta::OMEXMLMetadataRoot> root(std::dynamic_pointer_cast<ome::xml::meta::OMEXMLMetadataRoot>(omexml.getRoot()));
      if (root)
        {
          std::vector<std::shared_ptr<ome::xml::model::Image>>& images(root->getImageList());
          for(std::vector<std::shared_ptr<ome::xml::model::Image>>::const_iterator image = images.begin();
              image != images.end();
              ++image)
            {
              std::shared_ptr<ome::xml::model::Pixels> pixels((*image)->getPixels());
              if (pixels)
                {
                  // Note a copy not a reference to avoid iterator
                  // invalidation during removal.
                  std::vector<std::shared_ptr<ome::xml::model::TiffData>> tiffData(pixels->getTiffDataList());
                  for (std::vector<std::shared_ptr<ome::xml::model::TiffData>>::iterator tiff = tiffData.begin();
                   tiff != tiffData.end();
                       ++tiff)
                    {
                      pixels->removeTiffData(*tiff);
                    }
                  std::shared_ptr<ome::xml::model::MetadataOnly> metadataOnly;
                  pixels->setMetadataOnly(metadataOnly);
                }
            }
        }
    }

    void
    removeChannels(::ome::xml::meta::OMEXMLMetadata& omexml,
                   dimension_size_type               image,
                   dimension_size_type               sizeC)
    {
      omexml.resolveReferences();
      std::shared_ptr<ome::xml::meta::OMEXMLMetadataRoot> root(std::dynamic_pointer_cast<ome::xml::meta::OMEXMLMetadataRoot>(omexml.getRoot()));
      if (root)
        {
          std::shared_ptr<ome::xml::model::Image>& imageref(root->getImage(image));
          if (image)
            {
              std::shared_ptr<ome::xml::model::Pixels> pixels(imageref->getPixels());
              if (pixels)
                {
                  std::vector<std::shared_ptr<ome::xml::model::Channel>> channels(pixels->getChannelList());
                  for (Metadata::index_type c = 0U; c < channels.size(); ++c)
                    {
                      std::shared_ptr<ome::xml::model::Channel> channel(channels.at(c));
                      if (channel->getID().empty() || c >= sizeC)
                        pixels->removeChannel(channel);
                    }
                }
            }
        }
    }

    MetadataMap
    getOriginalMetadata(::ome::xml::meta::OMEXMLMetadata& omexml)
    {
      MetadataMap map;

      std::shared_ptr<ome::xml::meta::OMEXMLMetadataRoot> root(std::dynamic_pointer_cast<ome::xml::meta::OMEXMLMetadataRoot>(omexml.getRoot()));
      if (root)
        {
          std::shared_ptr<StructuredAnnotations> sa(root->getStructuredAnnotations());
          if (sa)
            {
              for (OMEXMLMetadata::index_type i = 0; i < sa->sizeOfXMLAnnotationList(); ++i)
                {
                  // Check if this is an OriginalMetadataAnnotation object.
                  std::shared_ptr<XMLAnnotation> annotation(sa->getXMLAnnotation(i));
                  std::shared_ptr<OriginalMetadataAnnotation> original(std::dynamic_pointer_cast<OriginalMetadataAnnotation>(annotation));
                  if (original)
                    {
                      const OriginalMetadataAnnotation::metadata_type kv(original->getMetadata());
                      map.set(kv.first, kv.second);
                      continue;
                    }

                  // Fall back to parsing by hand.
                  try
                    {
                      std::string wrappedValue("<wrapped>");
                      wrappedValue += annotation->getValue();
                      wrappedValue += "</wrapped>";

#ifdef OME_HAVE_XERCES_DOM
                      common::xml::Platform xmlplat;
                      common::xml::dom::ParseParameters params;
                      params.validationScheme = xercesc::XercesDOMParser::Val_Never;
#endif
                      DOMDocument doc(ome::xml::createDocument(wrappedValue));

                      std::vector<DOMElement> OriginalMetadataValue_nodeList =
                        ome::xml::model::detail::OMEModelObject::getChildrenByTagName(
#ifdef OME_HAVE_XERCES_DOM
                                                                                      doc.getDocumentElement(),
#elif OME_HAVE_QT5_DOM
                                                                                      doc.documentElement(),
#endif
                                                                                      "OriginalMetadata"
                                                                                      );
                      if (OriginalMetadataValue_nodeList.size() > 1)
                        {
                          std::string fs = fmt::format("Value node list size {0} != 1",
                                                       OriginalMetadataValue_nodeList.size());
                          throw ModelException(fs);
                        }
                      else if (OriginalMetadataValue_nodeList.size() != 0)
                        {
                          OriginalMetadataAnnotation::metadata_type kv;
                          std::vector<DOMElement> Key_nodeList =
                            ome::xml::model::detail::OMEModelObject::getChildrenByTagName(OriginalMetadataValue_nodeList.at(0),
                                                                                          "Key");
                          if (Key_nodeList.size() > 1)
                            {
                              std::string fs = fmt::format("Key node list size {0} != 1",
                                                           Key_nodeList.size());
                              throw ModelException(fs);
                            }
                          else if (Key_nodeList.size() != 0)
                            {
#ifdef OME_HAVE_XERCES_DOM
                              kv.first = Key_nodeList.at(0).getTextContent();
#elif OME_HAVE_QT5_DOM
                              QDomNode node = Key_nodeList.at(0).firstChild();
                              if (!node.isNull())
                                {
                                  QDomText text_content = node.toText();
                                  if (!text_content.isNull())
                                    kv.first = text_content.data().toStdString();
                                }
#endif
                            }
                          std::vector<DOMElement> Value_nodeList =
                            ome::xml::model::detail::OMEModelObject::getChildrenByTagName(OriginalMetadataValue_nodeList.at(0),
                                                                                          "Value");
                          if (Value_nodeList.size() > 1)
                            {
                              std::string fs = fmt::format("Value node list size {0} != 1",
                                                           Value_nodeList.size());
                              throw ModelException(fs);
                            }
                          else if (Value_nodeList.size() != 0)
                            {
#ifdef OME_HAVE_XERCES_DOM
                              kv.second = Value_nodeList.at(0).getTextContent();
#elif OME_HAVE_QT5_DOM
                              QDomNode node = Value_nodeList.at(0).firstChild();
                              if (!node.isNull())
                                {
                                  QDomText text_content = node.toText();
                                  if (!text_content.isNull())
                                    kv.second = text_content.data().toStdString();
                                }
#endif
                            }
                          map.set(kv.first, kv.second);
                          continue;
                        }
                    }
                  catch (const std::exception&)
                    {
                      /// @todo log error
                    }
                }
            }
        }

      return map;
    }

    void
    fillOriginalMetadata(::ome::xml::meta::OMEXMLMetadata& omexml,
                         const MetadataMap&                metadata)
    {
      omexml.resolveReferences();

      if (metadata.empty())
        return;

      MetadataMap flat(metadata.flatten());

      std::shared_ptr<ome::xml::meta::OMEXMLMetadataRoot> root(std::dynamic_pointer_cast<ome::xml::meta::OMEXMLMetadataRoot>(omexml.getRoot()));
      if (root)
        {
          std::shared_ptr<StructuredAnnotations> sa(root->getStructuredAnnotations());
          if (!sa)
            sa = std::make_shared<StructuredAnnotations>();
          OMEXMLMetadata::index_type annotationIndex = sa->sizeOfXMLAnnotationList();
          OMEXMLMetadata::index_type idIndex = sa->sizeOfXMLAnnotationList();

          std::set<std::string> ids;
          for (OMEXMLMetadata::index_type i = 0; i < annotationIndex; ++i)
            {
              // Already in metadata store
              ids.insert(omexml.getXMLAnnotationID(i));
            }

          for (MetadataMap::const_iterator i = flat.begin();
               i != flat.end();
               ++i, ++annotationIndex)
            {
              std::string id;
              do
                {
                  id = createID("Annotation", idIndex);
                  ++idIndex;
                }
              while (ids.find(id) != ids.end());

              std::ostringstream value;
              ome::compat::visit(::ome::files::detail::MetadataMapValueTypeOStreamVisitor(value), i->second);

              std::shared_ptr<OriginalMetadataAnnotation> orig(std::make_shared<OriginalMetadataAnnotation>());
              orig->setID(id);
              orig->setMetadata(OriginalMetadataAnnotation::metadata_type(i->first, value.str()));
              std::shared_ptr<XMLAnnotation> xmlorig(std::static_pointer_cast<XMLAnnotation>(orig));
              sa->addXMLAnnotation(xmlorig);
            }

          root->setStructuredAnnotations(sa);
        }
    }

    std::string
    getModelVersion()
    {
      return OME_XML_MODEL_VERSION;
    }

    std::string
    getModelVersion(DOMDocument& document)
    {
#ifdef OME_HAVE_XERCES_DOM
      DOMElement docroot(document.getDocumentElement());
      std::string ns = common::xml::String(docroot->getNamespaceURI());
#elif OME_HAVE_QT5_DOM
      DOMElement docroot(document.documentElement());
      std::string ns = docroot.namespaceURI().toStdString();
#endif

      ome::compat::smatch found;

      if (ome::compat::regex_match(ns, found, schema_match))
        {
          return found[1];
        }
      else if(ns == "http://www.openmicroscopy.org/XMLschemas/OME/FC/ome.xsd")
        {
          return "2003-FC";
        }

      return "";
    }

    std::string
    getModelVersion(const std::string& document)
    {
#ifdef OME_HAVE_XERCES_DOM
      ome::common::xml::Platform xmlplat;

      std::shared_ptr<xercesc::SAX2XMLReader> parser(xercesc::XMLReaderFactory::createXMLReader());
      // We only want to get the schema version, so disable checking
      // of schema etc.  If there are problems with the XML, they'll
      // be picked up when we parse it for real.  Here, we'll only
      // read the first element if it's a valid OME-XML document.
      parser->setFeature(xercesc::XMLUni::fgSAX2CoreValidation, false);
      parser->setFeature(xercesc::XMLUni::fgXercesSchemaFullChecking, false);
      parser->setFeature(xercesc::XMLUni::fgXercesLoadSchema, false);
      // Needed to get the schema namespace.
      parser->setFeature(xercesc::XMLUni::fgSAX2CoreNameSpaces, true);

      xercesc::MemBufInputSource source(reinterpret_cast<const XMLByte *>(document.c_str()),
                                        static_cast<XMLSize_t>(document.size()),
                                        common::xml::String("OME-XML model version"));

      OMEXMLVersionParser handler;
      parser->setContentHandler(&handler);

      try
        {
          parser->parse(source);
        }
      catch (const xercesc::XMLException& e)
        {
          ome::common::xml::String message(e.getMessage());
          std::cerr << "XMLException parsing schema version: " << message << '\n';
          return "";
        }
      catch (const xercesc::SAXParseException& e)
        {
          ome::common::xml::String message(e.getMessage());
          std::cerr << "SAXParseException parsing schema version: " << message << '\n';
          return "";
        }
      catch (const xercesc::SAXException&)
        {
          // Early termination (expected).
        }
      catch (...)
        {
          std::cerr << "Unexpected Exception parsing schema version\n";
          return "";
        }
#elif OME_HAVE_QT5_DOM
      QXmlSimpleReader parser;
      OMEXMLVersionParser handler;
      parser.setContentHandler(&handler);

      QByteArray raw_data(document.c_str(), document.size());
      QXmlInputSource source;
      source.setData(raw_data);
      parser.parse(&source);
#endif

      return handler.getVersion();
    }

    std::string
    transformToLatestModelVersion(const std::string& document)
    {
      ome::xml::OMEEntityResolver entity_resolver;
      ome::xml::OMETransformResolver transform_resolver;

      std::string upgraded_xml;
      ome::xml::transform(OME_XML_MODEL_VERSION, document, upgraded_xml,
                          entity_resolver, transform_resolver);

      return upgraded_xml;
    }

    bool
    defaultCreationDateEnabled()
    {
      return defaultCreationDate;
    }

    void
    defaultCreationDateEnabled(bool enabled)
    {
      defaultCreationDate = enabled;
    }

    void
    setDefaultCreationDate(::ome::xml::meta::MetadataStore&     store,
                           dimension_size_type                  series,
                           const ome::compat::filesystem::path& id)
    {
      if (defaultCreationDateEnabled())
        {
          Timestamp cdate;
          if (exists(id))
            cdate = ome::compat::convert_file_time(ome::compat::filesystem::last_write_time(id));
          store.setImageAcquisitionDate(cdate, series);
        }
    }

    ome::xml::model::enums::DimensionOrder
    createDimensionOrder(const std::string& order)
    {
      // A set could be used here, but given the tiny string length, a
      // linear scan is quicker than an index lookup.

      static const std::string validchars("XYZTC");

      std::string validorder;

      for (const auto& o : order)
        {
          if (validchars.find_first_of(o) != std::string::npos &&
              validorder.find_first_of(o) == std::string::npos)
              validorder += o;
        }

      for (auto c : validchars)
        {
          if (validorder.find_first_of(c) == std::string::npos)
            validorder += c;
        }

      return ome::xml::model::enums::DimensionOrder(validorder);
    }

    storage_size_type
    pixelSize(const ::ome::xml::meta::MetadataRetrieve& meta,
              dimension_size_type                       series)
    {
      dimension_size_type x = meta.getPixelsSizeX(series);
      dimension_size_type y = meta.getPixelsSizeY(series);
      dimension_size_type z = meta.getPixelsSizeZ(series);
      dimension_size_type t = meta.getPixelsSizeT(series);
      dimension_size_type c = meta.getPixelsSizeC(series);

      storage_size_type size = bytesPerPixel(meta.getPixelsType(series));
      size *= x;
      size *= y;
      size *= z;
      size *= t;
      size *= c;

      return size;
    }

    storage_size_type
    pixelSize(const ::ome::xml::meta::MetadataRetrieve& meta)
    {
      storage_size_type size = 0;

      for (dimension_size_type  s = 0;
           s < meta.getImageCount();
           ++s)
        {
          size += pixelSize(meta, s);
        }

      return size;
    }

    storage_size_type
    significantPixelSize(const ::ome::xml::meta::MetadataRetrieve& meta,
                         dimension_size_type                       series)
    {
      dimension_size_type x = meta.getPixelsSizeX(series);
      dimension_size_type y = meta.getPixelsSizeY(series);
      dimension_size_type z = meta.getPixelsSizeZ(series);
      dimension_size_type t = meta.getPixelsSizeT(series);
      dimension_size_type c = meta.getPixelsSizeC(series);

      storage_size_type size = significantBitsPerPixel(meta.getPixelsType(series));
      size *= x;
      size *= y;
      size *= z;
      size *= t;
      size *= c;

      return size;
    }

    storage_size_type
    significantPixelSize(const ::ome::xml::meta::MetadataRetrieve& meta)
    {
      storage_size_type size = 0;

      for (dimension_size_type  s = 0;
           s < meta.getImageCount();
           ++s)
        {
          size += pixelSize(meta, s);
        }

      return size;
    }

  }
}
